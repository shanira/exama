<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m170720_053640_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
			'title' => $this->string(),
			'body' => $this->string(),
			'category' => $this->string(),
			'author' => $this->string(),
			'ststus' => $this->string(),
			'created_at' => $this->date(),
			'updated_at' => $this->date(),
			'created_by' => $this->string(),
			'updated_by' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post');
    }
}
