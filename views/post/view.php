<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body',
            //'category',
            //'author',
			
						[
				'attribute' => 'category',
				'label' => 'category',
				'value' => function($model){
							return $model->categoryItem->category_name;
					},		
			],
            [
				'attribute' => 'ststus',
				'label' => 'status',
				'value' => function($model){
							return $model->statusItem->status_name;
					},		
			],
           // 'category',
		   [
		   
				'attribute' => 'author',
				'label' => 'author',
				
				'value' => function($model){
							return $model->authorItem->name;
					},		
			],
            //'ststus',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
